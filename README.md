# wright

String substitution language.

This is an attempt to write a minimal, "mustache not bad" spec. The specification is defined to be syntax-agnostic (since mustache is horrible for interpolating inside languages which use braces often), but I do define a mustache-like syntax later.

The specification and any associated code in this repository is released to the public domain to the greatest extent possible via the [CC0 License].

Thank you to [Emi Socks] for helping review the original specification and for writing an example implementation in Haskell!

[CC0 License]: ./LICENSE.md
[Emi Socks]: https://git.emisocks.com/socks/wright

## Data spec

There are three types of data:

* Strings
* Pairs
* Lists

### Strings

Strings must be valid Unicode, but have no other restrictions. In practice, this means that they can be encoded however you wish, but must be encodable as a sequence of Unicode code points.

### Pairs

Pairs are composed of a string key and a value, which can be a string or list (not a pair). In addition to both being valid Unicode, the key has the additional restriction that it must be a valid [UAX 31 identifier](https://www.unicode.org/reports/tr31/#D1) when normalized to [Normalization Form KC](https://www.unicode.org/reports/tr15/). In practice, this means that they must start with a code point with the `XID_Start` property, followed by zero or more code points that have the `XID_Continue` property. A corrollary of this is that the keys must also be nonempty.

### Lists

Lists contain zero or more elements, which can be a mix of strings and pairs; strings and pairs can also be called items. While a user can further constrain lists to contain either only pairs or strings, this is not a strict requirement of the spec. The order of elements in a list is always preserved.

### Example syntax

While data is defined in a syntax-independent way, a standard syntax can be defined loosely such that:

* String data is surrounded by quotation marks (U+22)
* Quotation marks inside quoted strings may be escaped with slashes (U+5C)
* Literal slashes at the end of a quoted string may be escaped by doubling
* Pairs are written as an unquoted string, a colon (U+3A), and a regular string
* Lists are surrounded by brackets (U+5B & U+5D)
* Items within lists are separated by commas (U+2C)
* Whitespace (White_Space) outside quotes is insignificant but may not occur inside a key

## Path spec

Paths to data can be represented by a sequence of keys or wildcards, called components, and may be empty. Each component is marked as strict or loose. 

### Lookups

Paths are used to get portions of data inside a larger piece of data. When looking up a path, the result is always a list, meaning that strings and pairs will be wrapped inside lists instead of returned verbatim.

The lookup logic is:

1. While the path is nonempty:
    1. Set the mode to strict or loose depending on how the first component is marked.
    2. If the data is a string:
        1. If the mode is loose, the component is a wildcard, or the component matches the string, remove the first component and continue the loop.
        2. Otherwise, return an empty list.
    3. If the data is a pair:
        1. If the component is a wildcard or the component matches the key, replace the data with its value, remove the first component, and continue the loop.
        2. If the mode is loose, remove the first component and continue the loop.
        3. If the mode is strict, return an empty list.
    4. If the data is a list:
        1. Return a list of the results for the path applied to each item in
           the list. Since lists may not contain other lists, the result must
           be flattened to remove nesting.
2. If the above didn't return any results:
    1. If the data is a string or pair, return a list containing that string or pair.
    2. If the data is a list, return the data as-is.

### Example syntax

While paths are defined in a syntax-independent way, a standard syntax can be defined loosely such that:

* Strict components are preceded by a single period (U+2E)
* Loose components are preceded by a single comma (U+2C)
* If the first component is strict, the period may be omitted
* Wildcards are written as an asterisk (U+2A)
* Keys are written verbatim
* Whitespace (White_Space) is insignificant but may not occur inside a key
* A single period may be parsed as an empty path; this can help with syntaxes that require a nonempty string for paths

## Template spec

String templates are sequences of one of five things:

* Comments
* Guards
* Scopes
* Slots
* Text

Comments are entirely excluded before processing and exist for those reading a template, not its output.

All templates are evaluated with a piece of data.

### Guards

Guards consist of three parts:

* The condition, a path
* The main branch, a template
* The fallback branch, a template

While the spec treats all guards as combined "if-else" blocks, syntaxes may choose to offer separate constructs for "if" and "unless" blocks. In these cases, either the main branch or fallback branch would be empty.

When evaluating a guard, the condition path is looked up in the template's data. If the lookup yields an empty list, the fallback branch is evaluated with the data for the entire template. If the lookup yields a nonempty list, the main branch is evaluated with the data at the given path. Note that guards are only evaluated once for a list of items, and not once for each item in a list.

### Scopes

Scopes consist of two parts:

* The bound, a path
* The body, a template

Scopes are the primary looping construct of templates, and can act very similarly to guards when applied to paths which yield only one item. For a list with zero or one items, a scope will act identically to a guard with an empty fallback branch.

When evaluating a scope, the bound is looked up in the template's data. If the lookup yields an empty list, the scope evaluates to an empty string. Otherwise, the scope is evaluated for each item in the lookup, then concatenated in the order of the lookup.

### Slots

Slots consist of a path to a string being inserted into the template. To evaluate a slot, this path must be looked up on the data applied to the template.

Slots evaluate differently depend on the result of this lookup:

* If an empty list, return an empty string.
* If a list with more than one item, return an error. Template processing may continue, but an empty string should be output in place of the slot rather than an arbitrary item in the list.
* If a list with a single string, return that string.
* If a list with a single pair, return the key for the pair.

### Text

Verbatim text inside templates is inserted as-is. Templates may offer the ability to control how text is interpreted around comments, guards, and slots (for example, adding or removing whitespace) although this is not strictly part of the spec.

### Example syntax

While templates are defined in a syntax-independent way, a standard syntax can be defined loosely such that:

* Three consecutive braces of the same type (U+7B or U+7D) can be included to
  represent the verbatim text of two consecutive braces
* Any unpaired set of two consecutive braces is seen as a syntax error; all double-pairs that aren't triple-pairs are seen as syntax
* Positive guards consist of `{{` `?` path `}}`, a main template, then `{{` `/` path `}}`; the path in the closing tag must match the opening tag, but can also be omitted
* Negative guards consist of `{{` `!` path `}}`, a fallback template, then `{{` `/` path `}}`; the path in the closing tag must match the opening tag, but can also be omitted
* Scopes consist of `{{` `#` path `}}`, a body template, then `{{` `/` path `}}`; the path in the closing tag must match the opening tag, but can also be omitted
* Slots consist of `{{` path `}}`
* Comments consist of `{{` `=` `}}`, the comment, then `{{` `/` `}}`
