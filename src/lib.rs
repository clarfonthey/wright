//! String substitution language.
#![feature(unsized_fn_params)]

#[cfg(feature = "alloc")]
extern crate alloc;

use core::fmt;
use core::mem;

#[track_caller]
fn assert_key_valid(key: &str) {
    todo!()
}

/// Reference to a key.
#[derive(PartialEq, Eq, PartialOrd, Ord, Hash)]
#[repr(transparent)]
pub struct KeyRef(str);
impl KeyRef {
    /// Creates a key from a string reference.
    #[inline]
    pub fn from_str(s: &str) -> &KeyRef {
        assert_key_valid(s);
        unsafe { mem::transmute(s) }
    }

    /// Creates a key from a mutable string reference.
    #[inline]
    pub fn from_mut_str(s: &mut str) -> &mut KeyRef {
        assert_key_valid(s);
        unsafe { mem::transmute(s) }
    }

    /// Gets the string for the key.
    #[inline]
    pub fn as_str(&self) -> &str {
        &self.0
    }
}
impl fmt::Debug for KeyRef {
    #[inline]
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        fmt::Debug::fmt(&self.0, f)
    }
}
impl fmt::Display for KeyRef {
    #[inline]
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        fmt::Display::fmt(&self.0, f)
    }
}

/// Owned key.
#[cfg(feature = "alloc")]
#[derive(Clone, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub struct Key(String);
#[cfg(feature = "alloc")]
impl Key {
    /// Creates a key from a string reference.
    #[inline]
    pub fn from_str(s: &str) -> Key {
        Key::from_string(s.to_owned())
    }

    /// Creates a key from a string.
    #[inline]
    pub fn from_string(s: String) -> Key {
        assert_key_valid(&s);
        Key(s)
    }

    /// Gets the string for the key.
    #[inline]
    pub fn as_str(&self) -> &str {
        self.0.as_str()
    }

    /// Unwraps the key, returning the string.
    #[inline]
    pub fn into_string(self) -> String {
        self.0
    }

    /// Gets the key as a reference.
    #[inline]
    pub fn as_ref(&self) -> &KeyRef {
        KeyRef::from_str(&*self.0)
    }

    /// Gets the key as a reference.
    #[inline]
    pub fn as_mut(&mut self) -> &mut KeyRef {
        KeyRef::from_mut_str(&mut *self.0)
    }
}
#[cfg(feature = "alloc")]
impl fmt::Debug for Key {
    #[inline]
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        fmt::Debug::fmt(&self.0, f)
    }
}
#[cfg(feature = "alloc")]
impl fmt::Display for Key {
    #[inline]
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        fmt::Display::fmt(&self.0, f)
    }
}
#[cfg(feature = "alloc")]
impl<'a> From<&'a Key> for &'a KeyRef {
    #[inline]
    fn from(value: &'a Key) -> &'a KeyRef {
        value.as_ref()
    }
}

/// Reference to an item.
#[derive(Copy, Clone, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub enum ItemRef<'a, V> {
    /// A string.
    Str(&'a str),

    /// A key-value pair.
    Pair(&'a KeyRef, &'a V),
}
impl<'a, V> ItemRef<'a, V> {
    /// Gets the string for the item.
    ///
    /// For pairs, this is the key.
    #[inline]
    pub fn as_str(&self) -> &'a str {
        match self {
            ItemRef::Str(s) => s,
            ItemRef::Pair(k, _) => k.as_str(),
        }
    }

    /// Gets the string and value for the item.
    ///
    /// For strings, there is no value.
    #[inline]
    pub fn as_pair(&self) -> (&'a str, Option<&'a V>) {
        match self {
            ItemRef::Str(s) => (s, None),
            ItemRef::Pair(k, v) => (k.as_str(), Some(v)),
        }
    }
}
impl<V: fmt::Debug> fmt::Debug for ItemRef<'_, V> {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self {
            ItemRef::Str(s) => fmt::Debug::fmt(*s, f),
            ItemRef::Pair(s, v) => f.debug_tuple("").field(s).field(v).finish(),
        }
    }
}
impl<'a, V> fmt::Display for ItemRef<'a, V> {
    #[inline]
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        fmt::Debug::fmt(self.as_str(), f)
    }
}

/// Owned value.
#[cfg(feature = "alloc")]
#[derive(Clone, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub enum Item<V> {
    /// A string.
    String(String),

    /// A key-value pair.
    Pair(Key, V),
}
#[cfg(feature = "alloc")]
impl<V> Item<V> {
    /// Gets the string for the item.
    ///
    /// For pairs, this is the key.
    #[inline]
    pub fn as_str(&self) -> &str {
        match self {
            Item::String(s) => s,
            Item::Pair(k, _) => k.as_str(),
        }
    }

    /// Unwraps the item, returning the string.
    ///
    /// This discards the value if this is a pair.
    #[inline]
    pub fn into_string(self) -> String {
        match self {
            Item::String(s) => s,
            Item::Pair(k, _) => k.into_string(),
        }
    }

    /// Gets the string and value for the item.
    ///
    /// For strings, there is no value.
    #[inline]
    pub fn as_pair(&self) -> (&str, Option<&V>) {
        match self {
            Item::String(s) => (s, None),
            Item::Pair(k, v) => (k.as_str(), Some(v)),
        }
    }

    /// Unwraps the item, returning its string and value.
    ///
    /// For strings, there is no value.
    #[inline]
    pub fn into_pair(self) -> (String, Option<V>) {
        match self {
            Item::String(s) => (s, None),
            Item::Pair(k, v) => (k.into_string(), Some(v)),
        }
    }

    /// Gets the item as a reference.
    #[inline]
    pub fn as_ref(&self) -> ItemRef<'_, V> {
        match self {
            Item::String(s) => ItemRef::Str(s),
            Item::Pair(k, v) => ItemRef::Pair(k.as_ref(), v),
        }
    }
}
#[cfg(feature = "alloc")]
impl<V: fmt::Debug> fmt::Debug for Item<V> {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self {
            Item::String(s) => fmt::Debug::fmt(&*s, f),
            Item::Pair(s, v) => f.debug_tuple("").field(s).field(v).finish(),
        }
    }
}
#[cfg(feature = "alloc")]
impl<V> fmt::Display for Item<V> {
    #[inline]
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        fmt::Debug::fmt(self.as_str(), f)
    }
}
#[cfg(feature = "alloc")]
impl<'a, V> From<&'a Item<V>> for ItemRef<'a, V> {
    #[inline]
    fn from(value: &'a Item<V>) -> ItemRef<'a, V> {
        value.as_ref()
    }
}
